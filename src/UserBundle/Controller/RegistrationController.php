<?php
namespace UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class RegistrationController extends BaseController
{
    public function registerAction(Request $request)
    {
        $user = $this->getUser();
        if ($user) {
            return $this->redirectToRoute("my_profile", [
                'username'=> $user->getUsername(),
                'id'=> $user->getId()
        ]);
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        $cities = array();
        $loginError = $this->get('security.authentication_utils')->getLastAuthenticationError();

        $user = $userManager->createUser();
        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();

        $form->setData($user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setEnabled(true);
            $user->setProfilePicture("user_logo.png");
            $user->setFollowersCount(0);
            $user->setFollowedCount(0);
            $user->setMessagesNotSeen(0);
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('fos_user_register'));
        }

        return $this->render('AppBundle:Web/Pages:home.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
