<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Publication
 *
 * @ORM\Table(name="publication")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PublicationRepository")
 */
class Publication
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="publications")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var integer
     *
     * @ORM\Column(name="display", type="integer")
     */
    private $display;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="likes_count", type="integer")
     */
    private $likesCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="comments_count", type="integer")
     */
    private $commentsCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     * @ORM\Column(name="file_name", type="string", length=255, nullable=true)
     */
    protected $fileName;

    /**
     * @ORM\OneToMany(targetEntity="PublicationComment", mappedBy="publication")
     */
    private $publicationComments;

    


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Publication
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Publication
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->publicationComments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set likesCount
     *
     * @param integer $likesCount
     *
     * @return Publication
     */
    public function setLikesCount($likesCount)
    {
        $this->likesCount = $likesCount;

        return $this;
    }

    /**
     * Get likesCount
     *
     * @return integer
     */
    public function getLikesCount()
    {
        return $this->likesCount;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Publication
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add publicationComment
     *
     * @param \AppBundle\Entity\PublicationComment $publicationComment
     *
     * @return Publication
     */
    public function addPublicationComment(\AppBundle\Entity\PublicationComment $publicationComment)
    {
        $this->publicationComments[] = $publicationComment;

        return $this;
    }

    /**
     * Remove publicationComment
     *
     * @param \AppBundle\Entity\PublicationComment $publicationComment
     */
    public function removePublicationComment(\AppBundle\Entity\PublicationComment $publicationComment)
    {
        $this->publicationComments->removeElement($publicationComment);
    }

    /**
     * Get publicationComments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublicationComments()
    {
        return $this->publicationComments;
    }

    /**
     * Set commentsComments
     *
     * @param integer $commentsComments
     *
     * @return Publication
     */
    public function setCommentsComments($commentsComments)
    {
        $this->commentsComments = $commentsComments;

        return $this;
    }

    /**
     * Set commentsCount
     *
     * @param integer $commentsCount
     *
     * @return Publication
     */
    public function setCommentsCount($commentsCount)
    {
        $this->commentsCount = $commentsCount;

        return $this;
    }

    /**
     * Get commentsCount
     *
     * @return integer
     */
    public function getCommentsCount()
    {
        return $this->commentsCount;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     *
     * @return Publication
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set display
     *
     * @param integer $display
     *
     * @return Publication
     */
    public function setDisplay($display)
    {
        $this->display = $display;

        return $this;
    }

    /**
     * Get display
     *
     * @return integer
     */
    public function getDisplay()
    {
        return $this->display;
    }
}
