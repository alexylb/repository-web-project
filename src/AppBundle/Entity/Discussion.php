<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Discussion
 *
 * @ORM\Table(name="discussion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DiscussionRepository")
 */
class Discussion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_one", referencedColumnName="id")
     */
    private $userOne;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_two", referencedColumnName="id")
     */
    private $userTwo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_message_at", type="datetime", nullable=true)
     */
    private $lastMessageAt;

    /**
     * @var integer
     * @ORM\Column(name="not_seen", type="integer")
     */
    protected $notSeen;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="discussion")
     */
    private $messages;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastMessageAt
     *
     * @param \DateTime $lastMessageAt
     *
     * @return Discussion
     */
    public function setLastMessageAt($lastMessageAt)
    {
        $this->lastMessageAt = $lastMessageAt;

        return $this;
    }

    /**
     * Get lastMessageAt
     *
     * @return \DateTime
     */
    public function getLastMessageAt()
    {
        return $this->lastMessageAt;
    }

    /**
     * Set userOne
     *
     * @param \AppBundle\Entity\User $userOne
     *
     * @return Discussion
     */
    public function setUserOne(\AppBundle\Entity\User $userOne = null)
    {
        $this->userOne = $userOne;

        return $this;
    }

    /**
     * Get userOne
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserOne()
    {
        return $this->userOne;
    }

    /**
     * Set userTwo
     *
     * @param \AppBundle\Entity\User $userTwo
     *
     * @return Discussion
     */
    public function setUserTwo(\AppBundle\Entity\User $userTwo = null)
    {
        $this->userTwo = $userTwo;

        return $this;
    }

    /**
     * Get userTwo
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserTwo()
    {
        return $this->userTwo;
    }

    /**
     * Set notSeen
     *
     * @param integer $notSeen
     *
     * @return Discussion
     */
    public function setNotSeen($notSeen)
    {
        $this->notSeen = $notSeen;

        return $this;
    }

    /**
     * Get notSeen
     *
     * @return integer
     */
    public function getNotSeen()
    {
        return $this->notSeen;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add message
     *
     * @param \AppBundle\Entity\Message $message
     *
     * @return Discussion
     */
    public function addMessage(\AppBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \AppBundle\Entity\Message $message
     */
    public function removeMessage(\AppBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }
}
