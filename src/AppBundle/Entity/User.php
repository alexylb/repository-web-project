<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @ExclusionPolicy("all")
 */
class User extends BaseUser
{

    /**
     * @Recaptcha\IsTrue
     */
    public $recaptcha;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    protected $firstname;

    /**
     * @var string
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    protected $lastname;

    /**
     * @var \DateTime
     * @ORM\Column(name="birthdate", type="datetime")
     */
    protected $birthdate;

    /**
     * @var integer
     * @ORM\Column(name="gender", type="integer")
     */
    protected $gender;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    protected $city;

    /**
     * @var string
     * @ORM\Column(name="profilepicture", type="string", length=255, nullable=true)
     */
    protected $profilePicture;

    /**
     * @var string
     * @ORM\Column(name="coverpicture", type="string", length=255, nullable=true)
     */
    protected $coverPicture;

    /**
     * @var integer
     * @ORM\Column(name="followers_count", type="integer")
     */
    protected $followersCount;

    /**
     * @var integer
     * @ORM\Column(name="followed_count", type="integer")
     */
    protected $followedCount;

    /**
     * @var integer
     * @ORM\Column(name="messages_not_seen", type="integer")
     */
    protected $messagesNotSeen;


    /**
     * @ORM\OneToMany(targetEntity="Publication", mappedBy="user")
     */
    private $publications;

    /**
     * @ORM\OneToMany(targetEntity="PublicationComment", mappedBy="user")
     */
    private $publicationComments;

    /**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=true)
     */
    private $country;

    /**
     * @var string
     * @ORM\Column(name="username_secur", type="string", length=255, nullable=true)
     */
    protected $usernameSecur;

    /**
     * @var string
     * @ORM\Column(name="profile_picture_secur", type="string", length=255, nullable=true)
     */
    protected $profilePictureSecur;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="user")
     */
    private $receivedMessages;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="user")
     */
    private $sentMessages;

    /**
     * @ORM\OneToMany(targetEntity="Follower", mappedBy="user")
     */
    private $followers;

    /**
     * @ORM\OneToMany(targetEntity="Follower", mappedBy="user")
     */
    private $followeds;



    public function __construct()
    {

        parent::__construct();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     *
     * @return User
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     *
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return integer
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set profilePicture
     *
     * @param string $profilePicture
     *
     * @return User
     */
    public function setProfilePicture($profilePicture)
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }

    /**
     * Get profilePicture
     *
     * @return string
     */
    public function getProfilePicture()
    {
        return $this->profilePicture;
    }

    /**
     * Set coverPicture
     *
     * @param string $coverPicture
     *
     * @return User
     */
    public function setCoverPicture($coverPicture)
    {
        $this->coverPicture = $coverPicture;

        return $this;
    }

    /**
     * Get coverPicture
     *
     * @return string
     */
    public function getCoverPicture()
    {
        return $this->coverPicture;
    }

    /**
     * Add publication
     *
     * @param \AppBundle\Entity\Publication $publication
     *
     * @return User
     */
    public function addPublication(\AppBundle\Entity\Publication $publication)
    {
        $this->publications[] = $publication;

        return $this;
    }

    /**
     * Remove publication
     *
     * @param \AppBundle\Entity\Publication $publication
     */
    public function removePublication(\AppBundle\Entity\Publication $publication)
    {
        $this->publications->removeElement($publication);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublications()
    {
        return $this->publications;
    }

    /**
     * Add publicationComment
     *
     * @param \AppBundle\Entity\PublicationComment $publicationComment
     *
     * @return User
     */
    public function addPublicationComment(\AppBundle\Entity\PublicationComment $publicationComment)
    {
        $this->publicationComments[] = $publicationComment;

        return $this;
    }

    /**
     * Remove publicationComment
     *
     * @param \AppBundle\Entity\PublicationComment $publicationComment
     */
    public function removePublicationComment(\AppBundle\Entity\PublicationComment $publicationComment)
    {
        $this->publicationComments->removeElement($publicationComment);
    }

    /**
     * Get publicationComments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublicationComments()
    {
        return $this->publicationComments;
    }


    /**
     * Add receivedMessage
     *
     * @param \AppBundle\Entity\Message $receivedMessage
     *
     * @return User
     */
    public function addReceivedMessage(\AppBundle\Entity\Message $receivedMessage)
    {
        $this->receivedMessages[] = $receivedMessage;

        return $this;
    }

    /**
     * Remove receivedMessage
     *
     * @param \AppBundle\Entity\Message $receivedMessage
     */
    public function removeReceivedMessage(\AppBundle\Entity\Message $receivedMessage)
    {
        $this->receivedMessages->removeElement($receivedMessage);
    }

    /**
     * Get receivedMessages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReceivedMessages()
    {
        return $this->receivedMessages;
    }

    /**
     * Add sentMessage
     *
     * @param \AppBundle\Entity\Message $sentMessage
     *
     * @return User
     */
    public function addSentMessage(\AppBundle\Entity\Message $sentMessage)
    {
        $this->sentMessages[] = $sentMessage;

        return $this;
    }

    /**
     * Remove sentMessage
     *
     * @param \AppBundle\Entity\Message $sentMessage
     */
    public function removeSentMessage(\AppBundle\Entity\Message $sentMessage)
    {
        $this->sentMessages->removeElement($sentMessage);
    }

    /**
     * Get sentMessages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSentMessages()
    {
        return $this->sentMessages;
    }

    /**
     * Set country
     *
     * @param \AppBundle\Entity\Country $country
     *
     * @return User
     */
    public function setCountry(\AppBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set followersCount
     *
     * @param integer $followersCount
     *
     * @return User
     */
    public function setFollowersCount($followersCount)
    {
        $this->followersCount = $followersCount;

        return $this;
    }

    /**
     * Get followersCount
     *
     * @return integer
     */
    public function getFollowersCount()
    {
        return $this->followersCount;
    }

    /**
     * Set followedCount
     *
     * @param integer $followedCount
     *
     * @return User
     */
    public function setFollowedCount($followedCount)
    {
        $this->followedCount = $followedCount;

        return $this;
    }

    /**
     * Get followedCount
     *
     * @return integer
     */
    public function getFollowedCount()
    {
        return $this->followedCount;
    }

    /**
     * Add follower
     *
     * @param \AppBundle\Entity\Follower $follower
     *
     * @return User
     */
    public function addFollower(\AppBundle\Entity\Follower $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower
     *
     * @param \AppBundle\Entity\Follower $follower
     */
    public function removeFollower(\AppBundle\Entity\Follower $follower)
    {
        $this->followers->removeElement($follower);
    }

    /**
     * Get followers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * Add followed
     *
     * @param \AppBundle\Entity\Follower $followed
     *
     * @return User
     */
    public function addFollowed(\AppBundle\Entity\Follower $followed)
    {
        $this->followed[] = $followed;

        return $this;
    }

    /**
     * Remove followed
     *
     * @param \AppBundle\Entity\Follower $followed
     */
    public function removeFollowed(\AppBundle\Entity\Follower $followed)
    {
        $this->followed->removeElement($followed);
    }

    /**
     * Get followed
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowed()
    {
        return $this->followed;
    }

    /**
     * Get followeds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFolloweds()
    {
        return $this->followeds;
    }

    /**
     * Set usernameSecur
     *
     * @param string $usernameSecur
     *
     * @return User
     */
    public function setUsernameSecur($usernameSecur)
    {
        $this->usernameSecur = $usernameSecur;

        return $this;
    }

    /**
     * Get usernameSecur
     *
     * @return string
     */
    public function getUsernameSecur()
    {
        return $this->usernameSecur;
    }

    /**
     * Set profilePictureSecur
     *
     * @param string $profilePictureSecur
     *
     * @return User
     */
    public function setProfilePictureSecur($profilePictureSecur)
    {
        $this->profilePictureSecur = $profilePictureSecur;

        return $this;
    }

    /**
     * Get profilePictureSecur
     *
     * @return string
     */
    public function getProfilePictureSecur()
    {
        return $this->profilePictureSecur;
    }

    /**
     * Set messagesNotSeen
     *
     * @param integer $messagesNotSeen
     *
     * @return User
     */
    public function setMessagesNotSeen($messagesNotSeen)
    {
        $this->messagesNotSeen = $messagesNotSeen;

        return $this;
    }

    /**
     * Get messagesNotSeen
     *
     * @return integer
     */
    public function getMessagesNotSeen()
    {
        return $this->messagesNotSeen;
    }
}
