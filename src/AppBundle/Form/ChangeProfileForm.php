<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\User;

class ChangeProfileForm extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('firstName', TextType::class)
                ->add('lastName', TextType::class)
                ->add('city', TextType::class, ['required'=>false])
                ->add('country', EntityType::class, ['class'=>'AppBundle:Country', 'choice_label'=> 'name'])
                ->add('gender',ChoiceType::class,
                  array('choices' => array(
                    'Femme' => 0,
                    'Homme' => 1,
                    )
                  ))
                ->add('profilePicture', FileType::class, ['data_class' => null, 'required'=>false])
                ->add('send', SubmitType::class, ['label' => 'Modifier'])
                ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
      $resolver->setDefaults([
        //'data_class' => 'AppBundle\Entity\User'
         'data_class' => User::class,
      ]);
    }
}