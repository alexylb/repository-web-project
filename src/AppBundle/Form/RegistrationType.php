<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;

class RegistrationType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('firstName', TextType::class)
                ->add('lastName', TextType::class)
                ->add('birthdate', BirthdayType::class, array(
              'years'=> range(date('Y'), date('Y') - 70),
              'placeholder' => array(
                  'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
              )))
              ->add('gender',ChoiceType::class,
                array('choices' => array(
                  'Femme' => 0,
                  'Homme' => 1,
                  )
                ))
                ->add('recaptcha', EWZRecaptchaType::class);
    }

    public function getParent(){
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix(){
        return 'app_user_registration';
    }
}
