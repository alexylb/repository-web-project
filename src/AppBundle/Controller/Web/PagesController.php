<?php

namespace AppBundle\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Publication;
use AppBundle\Form\TextPublicationForm;
use AppBundle\Entity\User;
use AppBundle\Entity\Follower;
use AppBundle\Entity\Message;
use AppBundle\Entity\PublicationComment;
use AppBundle\Entity\PublicationLike;
use AppBundle\Form\ChangeProfileForm;
use AppBundle\Form\ChatForm;
use AppBundle\Form\CommentForm;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

use Doctrine\ORM\Query\ResultSetMappingBuilder;

class PagesController extends Controller
{
    /**
     * @Route("/home", name="homepage")
     */
    public function homepageAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      return $this->render('AppBundle:Web/Pages:home.html.twig', []);
    }

    /**
     * @Route("/news/", name="news")
     */
    public function newsAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $user = $this->getUser();

      if(empty($user->getUsernameSecur())) {
        $user->setUsernameSecur($user->getUsername());
        $user->setProfilePictureSecur($user->getProfilePicture());

        $em->persist($user);
        $em->flush();
      }
      if($user->getProfilePictureSecur() != $user->getProfilePicture()) {
        $user->setProfilePictureSecur($user->getProfilePicture());

        $em->persist($user);
        $em->flush();
      }

      $publications = $em->getRepository('AppBundle:Publication')->findPublicationsNews($user);

      $allLikedPublicationsList = $em->getRepository('AppBundle:PublicationLike')->findBy(['user'=>$user]);

      $likedPublicationsList = [];

      foreach ($allLikedPublicationsList as $allLikedPublications) {
        $likedPublicationsList[] = $allLikedPublications->getPublication()->getId();
      }

      return $this->render('AppBundle:Web/Pages:news.html.twig', [
       "likedPublications"=>$likedPublicationsList,
       "publications"=>$publications]);
    }


    /**
     * @Route("/my-profile/{username}/", name="my_profile")
     */
    public function my_profileAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $user = $this->getUser();

      if(empty($user->getUsernameSecur())) {
        $user->setUsernameSecur($user->getUsername());
        $user->setProfilePictureSecur($user->getProfilePicture());

        $em->persist($user);
        $em->flush();
      }
      if($user->getProfilePictureSecur() != $user->getProfilePicture()) {
        $user->setProfilePictureSecur($user->getProfilePicture());

        $em->persist($user);
        $em->flush();
      }

      $currentProfilePicture = $user->getProfilePicture();

      $id = $request->query->get('id');

      $query = $em->createQuery("SELECT p FROM AppBundle:Publication p WHERE p.user = $id and p.display = 1 ORDER BY p.createdAt DESC");
      $publications  = $query->getResult();

      $allLikedPublicationsList = $em->getRepository('AppBundle:PublicationLike')->findBy(['user'=>$user]);

      $likedPublicationsList = [];

      foreach ($allLikedPublicationsList as $allLikedPublications) {
        $likedPublicationsList[] = $allLikedPublications->getPublication()->getId();
      }

      $form = $this->createForm(ChangeProfileForm::class, $user);

      $form->handleRequest($request);

      if ($form->isSubmitted()) {

      if($form['profilePicture']->getData() != null){
        $profilePictureName = $user->getUsername().$user->getId().'.'.$form['profilePicture']->getData()->guessExtension();
        $form['profilePicture']->getData()->move(__DIR__.'/../../../../web/uploads/medias', $profilePictureName);
        $user->setProfilePicture($profilePictureName);
        $user->setProfilePictureSecur($profilePictureName);
        
      } else {
        $user->setProfilePicture($currentProfilePicture);
      }

        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute("my_profile", [
        'username'=> $user->getUsername(),
        'id'=> $user->getId()
        ]);
      }

      return $this->render('AppBundle:Web/Pages:myProfile.html.twig', [
        "form" => $form->createView(), 
        "likedPublications"=>$likedPublicationsList,
        "publications"=>$publications
        ]);
    }


    /**
     * @Route("/profile/{username}/", name="profile")
     */
    public function profileAction(Request $request, $username)
    {
      $em = $this->getDoctrine()->getManager();
      $user = $this->getUser();

      $userProfile = $em->getRepository('AppBundle:User')->findOneBy(['username'=>$username]);
      
      $id = $request->query->get('id');

      $query = $em->createQuery("SELECT p FROM AppBundle:Publication p WHERE p.user = $id and p.display = 1 ORDER BY p.createdAt DESC");
      $publications  = $query->getResult();

      $followedAccount = $em->getRepository('AppBundle:Follower')->findOneBy(['follower'=>$user,
                                                                            'followed'=> $userProfile]);

      $isFollowed = true;

      if ($followedAccount == null) {
        $isFollowed = false;
      }

      $areFriends = false;
      if ($isFollowed) {
        $followToo = $em->getRepository('AppBundle:Follower')->findOneBy(['follower'=>$userProfile,
                                                                            'followed'=> $user]);
        if ($followToo != null) {
          $areFriends = true;
        }
      }

      $allLikedPublicationsList = $em->getRepository('AppBundle:PublicationLike')->findBy(['user'=>$user]);

      $likedPublicationsList = [];

      foreach ($allLikedPublicationsList as $allLikedPublications) {
        $likedPublicationsList[] = $allLikedPublications->getPublication()->getId();
      }

      return $this->render('AppBundle:Web/Pages:profile.html.twig', [
       "userProfile" => $userProfile,
       "isFollowed"=>$isFollowed,
       "likedPublications"=>$likedPublicationsList,
       "publications"=>$publications,
       "areFriends"=>$areFriends]);
    }


    /**
     * @Route("/add-text-publication/{id}/{username}/", name="addTextPublication")
     */
    public function addTextPublicationAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $publication = new Publication();
      $form = $this->createForm(TextPublicationForm::class, $publication);

      $user = $this->getUser();

      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
        $publication->setCreatedAt(new \DateTime());
        $publication->setUser($user);
        $publication->setLikesCount(0);
        $publication->setCommentsCount(0);
        $publication->setDisplay(1);

        if($form['fileName']->getData() != null){

          $pictureName = $form['fileName']->getData()->getClientOriginalName().$user->getUsername().'.'.$form['fileName']->getData()->guessExtension();

          $form['fileName']->getData()->move(__DIR__.'/../../../../web/uploads/medias', $pictureName);

          $publication->setFileName($pictureName);
        
        }
        if ($publication->getContent() == null && $publication->getFileName() == null) {
          
        } else {
          $em->persist($publication);
          $em->flush();
        }
          

          return $this->redirectToRoute("my_profile", [
          'username'=> $user->getUsername(),
          'id'=> $user->getId()
          ]);
        } 
      return $this->render('AppBundle:Web/Pages:textPublication.html.twig', ["form" => $form->createView()]);
    }



    /**
     * @Route("/publication-info/{id}/", name="publicationInfo")
     */
    public function publicationInfoAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();

      $comment = new PublicationComment();
      $form = $this->createForm(CommentForm::class, $comment);

      $user = $this->getUser();

      $publication = $em->getRepository('AppBundle:Publication')->findOneBy(['id'=>$id]);
      $comments = $em->getRepository('AppBundle:PublicationComment')->findBy(['publication'=>$publication]);

      $allLikedPublicationsList = $em->getRepository('AppBundle:PublicationLike')->findBy(['user'=>$user]);

      $likedPublicationsList = [];

      foreach ($allLikedPublicationsList as $allLikedPublications) {
        $likedPublicationsList[] = $allLikedPublications->getPublication()->getId();
      }

      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
        $comment->setCreatedAt(new \DateTime());
        $comment->setUser($user);
        $comment->setPublication($publication);
        
        $em->persist($comment);
        $em->flush();

        $commentsList = $em->getRepository('AppBundle:PublicationComment')->findBy(['publication'=>$publication]);
      
        $publication->setCommentsCount(count($commentsList));

        $em->persist($publication);
        $em->flush();

        return $this->redirectToRoute("publicationInfo", [
          'id'=> $publication->getId()
          ]);
      } 

      return $this->render('AppBundle:Web/Pages:publicationInfo.html.twig', [
        "form" => $form->createView(), 
        "publication"=>$publication, 
        "likedPublications"=>$likedPublicationsList,
        "comments"=>$comments
        ]);
    }


    /**
     * @Route("/all-followers/", name="allFollowers")
     */
    public function allFollowersAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $user = $this->getUser();

      $followers = $em->getRepository('AppBundle:Follower')->findBy(['followed'=>$user]);

      $followersUser = [];
      
      foreach ($followers as $follower) {
        $followersUser[] = $follower->getFollower();
      }

      return $this->render('AppBundle:Web/Pages:allFollowers.html.twig', ["followers"=>$followersUser]);
    }



    /**
     * @Route("/all-followed/", name="allFollowed")
     */
    public function allFollowedAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $user = $this->getUser();

      $followedList = $em->getRepository('AppBundle:Follower')->findBy(['follower'=>$user]);

      $followedUser = [];
      
      foreach ($followedList as $followed) {
        $followedUser[] = $followed->getFollowed();
      }

      return $this->render('AppBundle:Web/Pages:allFollowed.html.twig', ["followedList"=>$followedUser]);
    }


    /**
     * @Route("/all-messages/", name="allMessages")
     */
    public function allMessagesAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $user = $this->getUser();

      $query = $em->createQuery("SELECT d FROM AppBundle:Discussion d WHERE d.userOne = :user or d.userTwo = :user ORDER BY d.lastMessageAt DESC");
      $query->setParameters(['user'=> $user]);
      $discussions  = $query->getResult();

      return $this->render('AppBundle:Web/Pages:allMessages.html.twig', ["discussions"=>$discussions]);
    }

    /**
     * @Route("/chat/{id}/", name="chat")
     */
    public function chatAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();

      $user = $this->getUser();

      $discussion = $em->getRepository('AppBundle:Discussion')->findOneBy(['id'=>$id]);

      if($discussion->getUserOne() == $user) {
        $otherUser = $discussion->getUserTwo();
      } else {
        $otherUser = $discussion->getUserOne();
      }

      $messages = $em->getRepository('AppBundle:Message')->findBy(['discussion'=>$discussion]);

      $message = new Message();
      $form = $this->createForm(ChatForm::class, $message);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $message->setSendAt(new \DateTime());
        $message->setSeen(false);
        $message->setSender($user);
        $message->setReceiver($otherUser);
        $message->setDiscussion($discussion);

        $em->persist($message);
        $em->flush();

        return $this->redirectToRoute("chat", [
         'id'=> $discussion->getId()
        ]);
      }

      return $this->render('AppBundle:Web/Pages:chat.html.twig', [
        "form" => $form->createView(),
        "discussion"=>$discussion, 
        "messages"=>$messages]);
    }

}
