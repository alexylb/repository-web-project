<?php

namespace AppBundle\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Publication;
use AppBundle\Form\TextPublicationForm;
use AppBundle\Entity\User;
use AppBundle\Entity\Follower;
use AppBundle\Entity\PublicationComment;
use AppBundle\Entity\PublicationLike;
use AppBundle\Form\ChangeProfileForm;
use AppBundle\Form\CommentForm;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

class PublicationController extends Controller
{
    /**
     * @Route("/publications/interact/{username}/{id}", name="publications_interact")
     */
    public function interactAction($username, $id)
    {
    	$em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $publication = $em->getRepository('AppBundle:Publication')->findOneBy([
            'id' => $id
        ]);

        $checker = $em->getRepository('AppBundle:PublicationLike')->findOneBy([
            'publication' => $publication,
            'user' => $user,
        ]);


        if(is_null($checker) || empty($checker)) {
            $this->like($username, $id);
        } else {
            $this->unlike($username, $id);
        }

        exit;
    }

    private function like($username, $id)
    {
      $em = $this->getDoctrine()->getManager();

      $user = $this->getUser();

      $publication = $em->getRepository('AppBundle:Publication')->findOneBy([
        'id' => $id
      ]);

      $publicationLike = new PublicationLike();

      $publicationLike->setUser($user);
      $publicationLike->setPublication($publication);

      $em->persist($publicationLike);
      $em->flush();

      $likesList = $em->getRepository('AppBundle:PublicationLike')->findBy(['publication'=>$publication]);
      
      $publication->setLikesCount(count($likesList));

      $em->persist($publication);
      $em->flush();
    }

    private function unlike($username, $id)
    {
      $em = $this->getDoctrine()->getManager();

      $user = $this->getUser();

      $publication = $em->getRepository('AppBundle:Publication')->findOneBy(['id'=>$id]);

      $publicationLike = $em->getRepository('AppBundle:PublicationLike')
      		->findOneBy(['publication'=>$publication, 'user'=>$user]);

      $em->remove($publicationLike);
      $em->flush();

      $likesList = $em->getRepository('AppBundle:PublicationLike')->findBy(['publication'=>$publication]);
      
      $publication->setLikesCount(count($likesList));

      $em->persist($publication);
      $em->flush();
    }
}