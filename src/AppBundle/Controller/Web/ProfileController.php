<?php

namespace AppBundle\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Publication;
use AppBundle\Form\TextPublicationForm;
use AppBundle\Entity\User;
use AppBundle\Entity\Discussion;
use AppBundle\Entity\Follower;
use AppBundle\Entity\PublicationComment;
use AppBundle\Entity\PublicationLike;
use AppBundle\Form\ChangeProfileForm;
use AppBundle\Form\CommentForm;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

class ProfileController extends Controller
{

	/**
     * @Route("/follow/{username}/", name="follow")
     */
    public function followAction(Request $request, $username)
    {
      $em = $this->getDoctrine()->getManager();

      $user = $this->getUser();

      $userProfile = $em->getRepository('AppBundle:User')->findOneBy(['username'=>$username]);

      $follower = new Follower();

      $follower->setFollower($user);
      $follower->setFollowed($userProfile);

      $em->persist($follower);
      $em->flush();

      $followedAccountsList = $em->getRepository('AppBundle:Follower')->findBy(['follower'=>$user]);
      $followerAccountsList = $em->getRepository('AppBundle:Follower')->findBy(['followed'=>$userProfile]);
      
      $user->setFollowedCount(count($followedAccountsList));
      $userProfile->setFollowersCount(count($followerAccountsList));

      
      $em->persist($user);
      $em->persist($userProfile);
      $em->flush();

      $isFollowedToo = $em->getRepository('AppBundle:Follower')->findOneBy(['follower'=>$userProfile, 'followed'=>$user]);

      if ($isFollowedToo != null) {
        $discussionExists = [];
        $discussionExists[] = $em->getRepository('AppBundle:Discussion')->findOneBy(['userOne'=>$userProfile, 'userTwo'=>$user]);
        $discussionExists[] = $em->getRepository('AppBundle:Discussion')->findOneBy(['userOne'=>$user, 'userTwo'=>$userProfile]);
        

        if ($discussionExists[0] == null && $discussionExists[1] == null) {
          $discussion = new Discussion();
          $discussion->setUserOne($user);
          $discussion->setUserTwo($userProfile);
          $discussion->setNotSeen(0);

          $em->persist($discussion);
          $em->flush();
        }
      }

      return $this->redirectToRoute("profile", [
          'username'=> $userProfile->getUsername(),
          'id'=> $userProfile->getId()
          ]);
    }

    /**
     * @Route("/unfollow/{username}/{where}/", name="unfollow")
     */
    public function unfollowAction(Request $request, $username, $where)
    {
      $em = $this->getDoctrine()->getManager();

      $user = $this->getUser();

      $userProfile = $em->getRepository('AppBundle:User')->findOneBy(['username'=>$username]);

      $followedAccount = $em->getRepository('AppBundle:Follower')->findOneBy(['follower'=>$user, 
                                                                            'followed'=>$userProfile]);

      $em->remove($followedAccount);
      $em->flush();
      
      $followedAccountsList = $em->getRepository('AppBundle:Follower')->findBy(['follower'=>$user]);
      $followerAccountsList = $em->getRepository('AppBundle:Follower')->findBy(['followed'=>$userProfile]);
      
      $user->setFollowedCount(count($followedAccountsList));
      $userProfile->setFollowersCount(count($followerAccountsList));

      $em->persist($user);
      $em->persist($userProfile);
      $em->flush();

      if($where == "followed") {
        return $this->redirectToRoute("allFollowed", []);
      } else {

        return $this->redirectToRoute("profile", [
          'username'=> $userProfile->getUsername(),
          'id'=> $userProfile->getId()
          ]);
      }

    }

    /**
     * @Route("/search", name="search")
     */
    public function searchAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $user = $this->getUser();

      $word = $request->request->get("search_word");

      $userProfile = $em->getRepository('AppBundle:User')->findOneBy(['username'=>$word]);

      if ($userProfile != null && $userProfile->getId() != $user->getId()) {
        return $this->redirectToRoute("profile", [
          'username'=> $userProfile->getUsername(),
          'id'=> $userProfile->getId()
          ]);
      } else {

        return $this->redirectToRoute("my_profile", [
        'username'=> $user->getUsername(),
        'id'=> $user->getId()
        ]);
      }

      
    }

}