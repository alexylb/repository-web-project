<?php

namespace AppBundle\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    /**
     * @Route("/login_redir", name="loginRedirection")
     */
    public function loginRedirectionAction(Request $request)
    {	
    	$user = $this->getUser();
      
      return $this->redirectToRoute("my_profile", [
      	'username'=> $user->getUsername(),
      	'id'=> $user->getId()
      	]);
    }

}