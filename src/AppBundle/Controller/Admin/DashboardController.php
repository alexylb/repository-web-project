<?php
namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Publication;
use AppBundle\Form\DealType;
use Cocur\Slugify\Slugify;


class DashboardController extends Controller
{
  /**
   * @Route("/", name="admin_dashboard")
   */
  public function indexAction(Request $request)
  {
    return $this->render('AppBundle:Admin/Dashboard:index.html.twig', []);
  }

  /**
   * @Route("/publications/", name="admin_dashboard_publications")
   */
  public function publicationsAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $publications = $em->getRepository('AppBundle:Publication')->findAll();

    $paginator  = $this->get('knp_paginator');
    $pagination = $paginator->paginate(
    $publications, /* query NOT result */
    $request->query->getInt('page', 1)/*page number*/,
    20/*limit per page*/
    );
    return $this->render('AppBundle:Admin/Dashboard:publications.html.twig', ['pagination' => $pagination]);
  }

  /**
   * @Route("/publications-user/{id}/", name="admin_dashboard_publications_user")
   */
  public function publicationsUserAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $userProfile = $em->getRepository('AppBundle:User')->find($id);

    $publications = $em->getRepository('AppBundle:Publication')->findBy(['user'=>$userProfile]);

    $paginator  = $this->get('knp_paginator');
    $pagination = $paginator->paginate(
    $publications, /* query NOT result */
    $request->query->getInt('page', 1)/*page number*/,
    20/*limit per page*/
    );
    return $this->render('AppBundle:Admin/Dashboard:publications.html.twig', ['pagination' => $pagination]);
  }

  /**
   * @Route("/publications/display/{id}/", name="admin_dashboard_publications_display")
   */
  public function annoncesDisplayAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();

    $publication = $this->getDoctrine()->getRepository('AppBundle:Publication')->find($id);
    $userProfile = $publication->getUser();
    
    if ($publication->getDisplay() == 0) {
      $publication->setDisplay(1);
    } else {
      $publication->setDisplay(0);
    }

    $em->persist($publication);
    $em->flush();

    return $this->redirectToRoute("admin_dashboard_publications_user", ['id'=>$userProfile->getId()]);
  }

  /**
   * @Route("/users/", name="admin_dashboard_users")
   */
  public function usersAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $users = $em->getRepository('AppBundle:User')->findAll();

    $paginator  = $this->get('knp_paginator');
    $pagination = $paginator->paginate(
    $users, /* query NOT result */
    $request->query->getInt('page', 1)/*page number*/,
    20/*limit per page*/
    );
    return $this->render('AppBundle:Admin/Dashboard:users.html.twig', ['pagination' => $pagination]);
  }

  /**
   * @Route("/users/display/{id}/", name="admin_dashboard_users_display")
   */
  public function usersDisplayAction(Request $request, $id)
  {
    $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
    $em = $this->getDoctrine()->getManager();
    if ($user->isEnabled() == 1) {
      $user->setEnabled(false);
      $user->setUsername("Profil supprimé");
      $user->setProfilePicture("user_logo.png");
    } else {
      $user->setEnabled(true);
      $user->setUsername($user->getUsernameSecur());
      $user->setProfilePicture($user->getProfilePictureSecur());
    }

    $em->persist($user);
    $em->flush();

    return $this->redirectToRoute("admin_dashboard_users");
  }

  /**
   * @Route("/comments/", name="admin_dashboard_comments")
   */
  public function commentsAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $comments = $em->getRepository('AppBundle:PublicationComment')->findAll();

    $paginator  = $this->get('knp_paginator');
    $pagination = $paginator->paginate(
    $comments, /* query NOT result */
    $request->query->getInt('page', 1)/*page number*/,
    20/*limit per page*/
    );
    return $this->render('AppBundle:Admin/Dashboard:comments.html.twig', ['pagination' => $pagination]);
  }

  /**
   * @Route("/comments/{id}/", name="admin_dashboard_comments_publication")
   */
  public function commentsPublicationAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $publication = $em->getRepository('AppBundle:Publication')->findOneBy(['id'=>$id]);
    $comments = $em->getRepository('AppBundle:PublicationComment')->findBy(['publication'=>$publication]);

    $paginator  = $this->get('knp_paginator');
    $pagination = $paginator->paginate(
    $comments, /* query NOT result */
    $request->query->getInt('page', 1)/*page number*/,
    20/*limit per page*/
    );
    return $this->render('AppBundle:Admin/Dashboard:comments.html.twig', ['pagination' => $pagination]);
  }

  /**
   * @Route("/comments/supp/{idpubli}/{idcomment}", name="admin_dashboard_comments_supp")
   */
  public function commentsSuppAction(Request $request, $idpubli, $idcomment)
  {
    $em = $this->getDoctrine()->getManager();
    $publication= $em->getRepository('AppBundle:Publication')->findOneBy(['id'=>$idpubli]);
    $comment = $em->getRepository('AppBundle:PublicationComment')->findOneBy(['id'=>$idcomment]);

    $em->remove($comment);
    $em->flush();

    $commentsList = $em->getRepository('AppBundle:PublicationComment')->findBy(['publication'=>$publication]);
      
    $publication->setCommentsCount(count($commentsList));

    $em->persist($publication);
    $em->flush();

    return $this->redirectToRoute("admin_dashboard_comments_publication", ['id'=>$publication->getId()]);
  }

}




?>
