<?php
namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Deal;
use AppBundle\Form\DealType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class DealsController extends FOSRestController{
  /**
  * @Rest\Get("/deals")
  */
  public function getDeal(Request $request){
    $em = $this->getDoctrine()->getManager();
    $deals = $em->getRepository("AppBundle:Deal")->findAll();

    return $deals;
  }

  /**
  * @Rest\Post("/deals")
  * @ApiDoc(
  *  description="Create a deal",
  *  input="AppBundle\Form\DealType"
  * )
  */
  public function addDeal(Request $request){

	$em = $this->getDoctrine()->getManager();

	$deal = new Deal();

    $form = $this->createForm(DealType::class, $deal, [
      "allow_extra_fields"=> true,
      "csrf_protection"=>false
    ]);
    $form->submit($request->request->all());

    if($form->isValid()){
      $userId = $request->request->get("user");

    	$user = $em->getRepository('AppBundle:User')->find($userId);

    	$deal->setUser($user);

    	$em->persist($deal);
        $em->flush();

		return $deal;
    } else {
    	return $form;
    }

  }
}
