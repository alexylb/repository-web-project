<?php
namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Deal;
use AppBundle\Form\DealType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class LoginController extends FOSRestController{
  /**
  * @Rest\Post("/login")
  * @ApiDoc(
  *  description="Login",
  * )
  */
  public function loginAction(Request $request){

      $em = $this->getDoctrine()->getManager();

      $manager = $this->get('fos_user.user_manager');
      $factory = $this->get('security.encoder_factory');

      $username = $request->request->get('username');
      $password = $request->request->get('password');

      $bool = false;

      $response = [
        'status' => false
      ];

      $user = $manager->findUserBy(["username"=> $username]);
      if(is_null($user)) {
        return $response;
      }
      $encoder = $factory->getEncoder($user);
      $bool = ($encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) ? true : false;

      if($bool) {
        $response = [
          'status' => true,
          'session' => [
            'id' => $user->getId(),
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
          ],
        ];
      }
      return $response;
    }

}
