<?php
namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Deal;
use AppBundle\Form\DealType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class DealsController extends FOSRestController{

  /**
  * @Rest\Post("/deals")
  * @ApiDoc(
  *  description="Create a deal",
  *  input="AppBundle\Form\DealType"
  * )
  */
  public function addComment(Request $request){

    $em = $this->getDoctrine()->getManager();

  	$comment = new DealComment();

      $form = $this->createForm(DealCommentType::class, $comment);
      $form->submit($request->request->all());

      if($form->isValid()){
          $userId = $request->request->get("user");
          $dealId = $request->request->get("deal");

      $user = $em->getRepository('AppBundle:User')->find($userId);
  		$deal = $em->getRepository('AppBundle:Deal')->find($dealId);

      	$comment->setUser($user);
        $comment->setDeal($deal);

      	$em->persist($comment);
        $em->flush();

  		    return $comment;

      } else {
      	return $form;
      }

  }
}
